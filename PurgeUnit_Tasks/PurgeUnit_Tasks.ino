#include <WiFi.h> // ESP32 WiFi include
#include <EnvironmentCalculations.h>
#include <BME280I2C.h>
#include <Wire.h>
#include "time.h"
#include <PubSubClient.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <EEPROM.h>

//0:PurgeOnPoint
//4:PurgeOffPoint
//8:

#define EEPROM_SIZE 12

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
// The pins for I2C are defined by the Wire-library. 
// On an arduino UNO:       A4(SDA), A5(SCL)
// On an arduino MEGA 2560: 20(SDA), 21(SCL)
// On an arduino LEONARDO:   2(SDA),  3(SCL), ...
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define SERIAL_BAUD 115200


BME280I2C::Settings settings(
   BME280::OSR_X16,
   BME280::OSR_X16,
   BME280::OSR_X16,
   BME280::Mode_Forced,
   BME280::StandbyTime_1000ms,
   BME280::Filter_Off,
   BME280::SpiEnable_False,
   BME280I2C::I2CAddr_0x76
);

BME280I2C::Settings settings2(
   BME280::OSR_X16,
   BME280::OSR_X16,
   BME280::OSR_X16,
   BME280::Mode_Forced,
   BME280::StandbyTime_1000ms,
   BME280::Filter_Off,
   BME280::SpiEnable_False,
   BME280I2C::I2CAddr_0x77
);

BME280I2C bme(settings);
BME280I2C bme2(settings2);

const char* mqtt_server = "10.1.1.3";
const char *SSID = "Radio-Noise S";
const char *WiFiPassword = "z4!9$*3wb3xxpyv2@gio";
#define WIFI_TIMEOUT_MS 10000 // 10 second WiFi connection timeout
#define WIFI_RECOVER_TIME_MS 5000 // Wait 5 seconds after a failed connection attempt

WiFiClient espClient;
const int purgepin =  27;    // the number of the purge pin
const int door1pin =  25;    // the number of the door1 pin
const int door2pin = 32;    // the number of the door2 pin

//float temp1, temp2, hum1, hum2, pres1, pres2;
float PurgeOnPoint = 0 , PurgeOffPoint =0;
int PostPurgeDelay = 0;
bool PurgeOn, HumTooHigh, DoorOpen, Door1Open, Door2Open, PostPurge;

struct SensorDataStruct {
  float temp1;
  float hum1;
  float pres1;
  float temp2;
  float hum2;
  float pres2;
};

SensorDataStruct sensordata;

void setup() {
  // put your setup code here, to run once:
  //Init EEPROM
  EEPROM.begin(EEPROM_SIZE);
  //EEPROM.write(0,PurgeOnPoint);
  //EEPROM.commit();
  //EEPROM.write(4,PurgeOffPoint);
  //EEPROM.commit();
  //EEPROM.write(8,PostPurgeDelay);
  //EEPROM.commit();
  PurgeOnPoint = EEPROM.read(0);
  PurgeOffPoint = EEPROM.read(4);
  PostPurgeDelay = EEPROM.read(8);
  pinMode(purgepin, OUTPUT);
  pinMode(door1pin, INPUT_PULLUP);
  pinMode(door2pin, INPUT_PULLUP);
  Serial.begin(SERIAL_BAUD);
  Serial.println(PurgeOnPoint);
  Serial.println(PurgeOffPoint);
  Serial.println(PostPurgeDelay);
  while(!Serial) {} // Wait

  
  Wire.begin();

  while(!bme.begin())
  {
    Serial.println("Could not find BME280 sensor #1");
    delay(1000);
  }
  while(!bme2.begin())
  {
    Serial.println("Could not find BME280 sensor #2");
    delay(1000);
  }

  // bme.chipID(); // Deprecated. See chipModel().
  switch(bme.chipModel())
  {
     case BME280::ChipModel_BME280:
       Serial.println("Found BME280 sensor #1! Success.");
       break;
     case BME280::ChipModel_BMP280:
       Serial.println("Found BMP280 sensor #1! No Humidity available.");
       break;
     default:
       Serial.println("Found UNKNOWN sensor #1! Error!");
  }

switch(bme2.chipModel())
  {
     case BME280::ChipModel_BME280:
       Serial.println("Found BME280 sensor #2! Success.");
       break;
     case BME280::ChipModel_BMP280:
       Serial.println("Found BMP280 sensor #2! No Humidity available.");
       break;
     default:
       Serial.println("Found UNKNOWN sensor #2! Error!");
  }

  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  delay(2000);
  display.clearDisplay();

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 10);
  // Display static text
  display.println("Hello, world!");
  display.display(); 

  xTaskCreate(
    updateDisplay,    // Function that should be called
    "Update Display",   // Name of the task (for debugging)
    2000,            // Stack size (bytes)
    NULL,            // Parameter to pass
    1,               // Task priority
    NULL             // Task handle
  );
  xTaskCreate(
    checkDoorStatus,    // Function that should be called
    "CheckDoorStatus",   // Name of the task (for debugging)
    2000,            // Stack size (bytes)
    NULL,            // Parameter to pass
    1,               // Task priority
    NULL             // Task handle
  );
  xTaskCreate(
    purgeControl,    // Function that should be called
    "PurgeControl",   // Name of the task (for debugging)
    2000,            // Stack size (bytes)
    NULL,            // Parameter to pass
    1,               // Task priority
    NULL             // Task handle
  );
  xTaskCreate(
    getEnvData,    // Function that should be called
    "getEnvData",   // Name of the task (for debugging)
    2000,            // Stack size (bytes)
    NULL,            // Parameter to pass
    1,               // Task priority
    NULL             // Task handle
  );
  xTaskCreate(
    checkHumidity,    // Function that should be called
    "checkHumidity",   // Name of the task (for debugging)
    2000,            // Stack size (bytes)
    NULL,            // Parameter to pass
    1,               // Task priority
    NULL             // Task handle
  );
  
}

void loop(){
  // put your main code here, to run repeatedly:
 printBME280Data(&Serial);
 Serial.println("Purge On: " + String(PurgeOn));
 Serial.println("Door Open: " + String(DoorOpen));
 Serial.println("Post Purge: " + String(PostPurge));
 
 //getEnvData();
 //checkDoorStatus();
 //checkHumidity(hum1);
 //purgeControl();
 //Serial.println(hum1);
 //updateDisplay();
 delay(1000);
}

void purgeControl(void * parameter){
  for(;;){
  if(DoorOpen == true){
    startPurge();
  }
  if(DoorOpen == false){
    if(HumTooHigh == true){
      startPurge();
    }
    if(HumTooHigh == false and PostPurge == false){
      stopPurge();
    }
  }
  vTaskDelay(500);
  }
}



void checkHumidity(void * parameter){
  
  for(;;){
    if ((sensordata.hum1 >= PurgeOnPoint) or (sensordata.hum2 >= PurgeOnPoint)){
      //startPurge();
      HumTooHigh = true;
    }
    else if ((sensordata.hum1 < PurgeOffPoint) and (sensordata.hum2 < PurgeOffPoint)){
      //stopPurge();
      HumTooHigh = false;
    }
  vTaskDelay(500);
  }
}

void checkDoorStatus(void * parameter){
  for(;;){
  if (digitalRead(door1pin) == HIGH){
    DoorOpen = true;
    Door1Open = true;
    PostPurge = true;
    }
  if (digitalRead(door2pin) == HIGH){
    DoorOpen = true;
    Door2Open = true;
    PostPurge = true;
    }
  if (digitalRead(door1pin) == LOW){
    Door1Open = false;
    }
  if (digitalRead(door2pin) == LOW){
    Door2Open = false;
    }
  if (digitalRead(door2pin) == LOW and digitalRead(door1pin) == LOW){
    DoorOpen = false;
    if(PostPurge == true){
      delay(PostPurgeDelay*1000);
      PostPurge = false;
    }
    }
    vTaskDelay(500);
  }
 }

 

void startPurge(){
  PurgeOn = true;
  digitalWrite(purgepin, HIGH);
}

void stopPurge(){
  PurgeOn = false;
  digitalWrite(purgepin, LOW);
}

void updateDisplay(void * parameter){
  for(;;){
  display.clearDisplay();

  if(PurgeOn==true){
    display.setTextSize(1);
    display.setCursor(90,0);
    display.print("HI");
  }
  if(DoorOpen == false and PostPurge==true){
    display.setTextSize(1);
    display.setCursor(115,0);
    display.print("PP");
  }
  if(Door1Open == true){
    display.setTextSize(1);
    display.setCursor(0,0);
    display.print("D1");
  }
  if(Door2Open == true){
    display.setTextSize(1);
    display.setCursor(15,0);
    display.print("D2");
  }
  display.drawRoundRect(90, 17, 38, 47, 2, WHITE);
  display.drawLine(90, 40 , 127, 40, WHITE);
  // display temperature
  display.setTextSize(1);
  display.setCursor(0,17);
  display.println("Temp: ");
  display.setTextSize(1);
  //display.setCursor(0,25);
  display.print(sensordata.temp1);
  display.print(" ");
  display.setTextSize(1);
  display.cp437(true);
  display.write(167);
  display.setTextSize(1);
  display.println("C");
  // display pressure
  display.setTextSize(1);
  //display.setCursor(0, 35);
  display.println("Pres: ");
  display.setTextSize(1);
  //display.setCursor(0, 43);
  display.print(sensordata.pres1/100);
  display.println(" hPa");
  display.setCursor(95, 25);
  display.print(sensordata.hum1);
  display.setCursor(95, 45);
  display.print(sensordata.hum2);
  display.display();
  vTaskDelay(500);
  }
}

void printLocalTime(){
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}

void getEnvData(void * parameter){
  for(;;){
  BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
  BME280::PresUnit presUnit(BME280::PresUnit_Pa);
  bme.read(sensordata.pres1, sensordata.temp1, sensordata.hum1, tempUnit, presUnit);
  bme2.read(sensordata.pres2, sensordata.temp2, sensordata.hum2, tempUnit, presUnit);
  vTaskDelay(500);
  }
}


void printBME280Data(
   Stream* client
){
   float temp(NAN), hum(NAN), pres(NAN);

   BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
   BME280::PresUnit presUnit(BME280::PresUnit_Pa);

   bme.read(pres, temp, hum, tempUnit, presUnit);
   client->print("BME1");
   client->print("Temp: ");
   client->print(temp);
   client->print("°"+ String(tempUnit == BME280::TempUnit_Celsius ? "C" :"F"));
   client->print("\t\tHumidity: ");
   client->print(hum);
   client->print("% RH");
   client->print("\t\tPressure: ");
   client->print(pres);
   client->print(" Pa");
   bme2.read(pres, temp, hum, tempUnit, presUnit);
   client->print("\nBME2");
   client->print("Temp: ");
   client->print(temp);
   client->print("°"+ String(tempUnit == BME280::TempUnit_Celsius ? "C" :"F"));
   client->print("\t\tHumidity: ");
   client->print(hum);
   client->print("% RH");
   client->print("\t\tPressure: ");
   client->print(pres);
   client->print(" Pa");
}
